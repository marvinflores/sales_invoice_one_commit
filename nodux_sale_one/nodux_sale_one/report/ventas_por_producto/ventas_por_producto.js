// Copyright (c) 2016, NODUX and contributors
// For license information, please see license.txt
/* eslint-disable */
frappe.query_reports["Ventas por Producto"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.defaults.get_default("year_start_date"),
			"width": "80"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": get_today()
		},
		{
			"fieldname":"product",
			"label": __("Producto"),
			"fieldtype": "Link",
			"options": "Item"
		}
	]
}
