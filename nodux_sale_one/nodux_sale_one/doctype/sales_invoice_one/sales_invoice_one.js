// Copyright (c) 2016, NODUX and contributors
// For license information, please see license.txt


frappe.ui.form.on('Sales Invoice One', {
	onload: function(frm) {
		var me = this;
		frm.refresh_fields();
		if (frm.doc.status == 'Draft' && frm.doc.docstatus=='0') {
			frm.call({
				method: "nodux_sale_one.nodux_sale_one.doctype.sales_invoice_one.sales_invoice_one.get_series",
				args:{
					'nota_venta':0,
				},
				callback: function(r) {
					if(!r.exc) {
						frm.set_value("naming_series", r.message);
						//set_field_options("naming_series", r.message);
					}
				}
			});
			frm.call({
				method: "nodux_sale_one.nodux_sale_one.doctype.sales_invoice_one.sales_invoice_one.get_sucursal",
				callback: function(r) {
					if(!r.exc) {
						frm.set_value("sucursal", r.message[0]);
						frm.set_value("dir_sucursal", r.message[1])
					}
				}
			});
		}
	},


	refresh: function(frm) {
		if (frm.doc.status == 'Draft' && frm.doc.docstatus=='0') {
			frm.add_custom_button(__("Pagar"), function() {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.doc.status == 'Confirmed' && frm.doc.docstatus=='0') {
			frm.add_custom_button(__("Pagar"), function() {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.doc.status == 'Confirmed' && frm.doc.docstatus=='1') {
			frm.add_custom_button(__("Pagar"), function() {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.doc.status == 'Done' && frm.doc.docstatus=='1') {
			frm.add_custom_button(__("Anular"), function() {
				frm.events.update_to_anulled_sale(frm);
			}).addClass("btn-primary");
		}
		frm.refresh_fields();
	},

	no_id:function(frm){
		if (frm.doc.no_id){
			frappe.db.get_value("Customer", {"tax_id": frm.doc.no_id}, ["customer_name","email","phone"], function(r) {
				frm.set_value("customer", r.customer_name);
				frm.set_value("correo_cliente", r.email);
				frm.set_value("telefono", r.phone);
			})
			frm.set_value("customer_name", frm.doc.cliente);
			frm.set_value("due_date", frappe.datetime.nowdate());
		}
		frm.refresh_fields();
	},
	customer: function(frm) {
		if (frm.doc.customer){
				frappe.db.get_value("Customer", {"customer_name": frm.doc.customer}, "tax_id", function(r) {
				frm.set_value("no_id", r.tax_id);
			})
			frappe.db.get_value("Customer", {"customer_name": frm.doc.customer}, "email", function(r) {
					frm.set_value("correo_cliente", r.email);
			})

			frm.set_value("customer_name", frm.doc.customer);
			frm.set_value("due_date", frappe.datetime.nowdate());
		}
		frm.refresh_fields();
	},

	nota_venta:function(frm){

		return frappe.call({
			doc: frm.doc,
			method: "get_series",
			args:{
			"nota_venta" : frm.doc.nota_venta
			},
			callback: function(r) {
				if(!r.exc) {
					//frm.set_value("naming_series", r.message);
					set_field_options("naming_series", r.message);
				}
				refresh_field("naming_series");
				frm.refresh_fields();
				frm.refresh();
			}
		});
	},

	update_to_quotation_sale: function(frm) {
		return frappe.call({
			doc: frm.doc,
			method: "update_to_quotation_sale",
			freeze: true,
			callback: function(r) {
				frm.refresh_fields();
				frm.refresh();
			}
		})
	},

	update_to_anulled_sale: function(frm) {
		var d = new frappe.ui.Dialog({
			title: __("Esta seguro de Anular la Venta"),
			fields: [
				{fieldname:"NO", "label":__("No"), "fieldtype":"Button"},
				{"fieldname":"coulmn_break_2","fieldtype":"Column Break"},
				{fieldname:"SI", "label":__("Si"), "fieldtype":"Button"}]
		});

		d.get_input("NO").on("click", function() {
			d.hide();
		});

		d.get_input("SI").on("click", function() {
			return frappe.call({
				doc: frm.doc,
				method: "update_to_anulled_sale",
				freeze: true,
				callback: function(r) {
					d.hide();
					frm.refresh_fields();
					frm.refresh();
				}
			})
		});
		d.show();
		frm.refresh_fields();
		frm.refresh();

	},

	update_to_pay_sale: function(frm) {
		var me = this;
		if (frm.doc.posting_date != frm.doc.due_date && frm.doc.status=="Draft"){
			total = 0.0
		}else{
			total = frm.doc.residual_amount
		}

		var d = new frappe.ui.Dialog({
			title: __("Payment"),
			fields: [
				{"fieldname":"customer", "fieldtype":"Link", "label":__("Customer"),
					options:"Customer", reqd: 1, label:"Customer", "default":frm.doc.customer_name},
				{"fieldname":"total", "fieldtype":"Currency", "label":__("Total Amount"),
					label:"Total Amount", "default":total},
				{"fieldname":"section_break","fieldtype":"Section Break"},
				{"fieldname":"recibo", "fieldtype":"Currency", "label":__("RECIBIDO"),
					label:"RECIBIDO", "default":frm.doc.residual_amount},
					{"fieldname":"coulmn_break_2","fieldtype":"Column Break"},
				{"fieldname":"cambio", "fieldtype":"Read Only", "label":__("CAMBIO"),
						label:"CAMBIO", "default":0},
				{"fieldname":"section_break","fieldtype":"Section Break"},
				{fieldname:"pay", "label":__("Pay"), "fieldtype":"Button"}]
		});

		d.get_input("recibo").on("change", function() {
			var values = d.get_values();
			var cambio = flt(values["recibo"] - values["total"])
			cambio = Math.round(cambio * 100) / 100
			d.set_value("cambio", cambio)
		});

		d.get_input("pay").on("click", function() {
			var values = d.get_values();
			if(!values) return;
			total =  Math.round(values["total"] * 100) / 100
			total_pay= Math.round(frm.doc.total * 100) / 100

			if(total < 0) frappe.throw("Ingrese monto a pagar");
			if(total < 0) frappe.throw("Monto a pagar no puede ser menor a 0");

			if(total > total_pay) frappe.throw("Monto a pagar no puede ser mayor al monto total de venta");
			return frappe.call({
				doc: frm.doc,
				method: "update_to_pay_sale",
				args: values,
				freeze: true,
				callback: function(r) {
					d.hide();
					refresh_field("payments");
					frm.refresh_fields();
					frm.refresh();
					// if (frm.doc.status == 'Done'){
					// 	// window.print();
					// 	// frappe.ui para reporte grid
					// 	// frappe.ui.get_print_settings(false, function(print_settings) {
					// 	// frappe.render_grid({grid: me.grid, title: me.page.title, print_settings: print_settings });
					// }
				}
			})
		});

		d.show();
		refresh_field("payments");
		frm.refresh_fields();
		frm.refresh();
	},

	print_document: function (html) {
		var w = window.open();
		w.document.write(html);
		w.document.close();
		setTimeout(function () {
			w.print();
			w.close();
		}, 1000)
	}
});

frappe.ui.form.on('Sales Invoice Item One', {
	item_name: function(frm, cdt, cdn) {
		var item = frappe.get_doc(cdt, cdn);
		if(!item.item_name) {
			item.item_name = "";
		} else {
			item.item_name = item.item_name;
		}
	},

    items_remove: function(frm, cdt, cdn){
		cur_frm.refresh_fields();
		calculate_base_imponible(frm);
	},

	barcode: function(frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if(d.barcode) {
			args = {
				'barcode'			: d.barcode
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_code_sale",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
		cur_frm.refresh_fields();
	},

	item_code: function(frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		var base_imponible = 0;
		var total_taxes = 0;
		var total = 0;
		var doc = frm.doc;

		if(d.item_code) {
			args = {
				'item_code'			: d.item_code,
				'qty'				: d.qty
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_details_sale",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm)
					}
				}
			});

			frm.refresh_fields();
		}
	},

	uom:function(frm, cdt, cdn){
		var d = locals[cdt][cdn];

		if(d.uom){
			args = {
				'item_code'	: d.item_code,
				'uom'			: d.uom,
				'stock_uom' : d.stock_uom,
				'conversion_factor': d.conversion_factor,
				'unit_price':d.unit_price,
				'qty'				: d.qty,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "update_unit",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}

	},

	subtotal_imp:function(frm, cdt, cdn){
		var d = locals[cdt][cdn];

		if(d.uom){
			args = {
				'item_code'		: d.item_code,
				'subtotal_imp': d.subtotal_imp,
				'qty'					: d.qty,
				'discount'		: d.discount,
				'unit_price'	: d.unit_price,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "calculate_desglose",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}

	},


	conversion_factor:function(frm, cdt, cdn){
		var d = locals[cdt][cdn];

		if(d.uom){
			args = {
				'item_code': d.item_code,
				'uom'			: d.uom,
				'stock_uom' : d.stock_uom,
				'conversion_factor': d.conversion_factor,
				'unit_price':d.unit_price,
				'qty'				: d.qty,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "update_unit",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}
	},

	qty: function(frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if(d.qty) {
			args = {
				'item_code'			: d.item_code,
				'qty'				: d.qty,
				'unit_price': d.unit_price
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_qty",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
	},

	unit_price: function(frm, cdt, cdn){
		// if user changes the rate then set margin Rate or amount to 0
		var d = locals[cdt][cdn];
		if(d.item_code){
			args = {
				'item_code'			: d.item_code,
				'qty'				: d.qty,
				'unit_price': d.unit_price,
				'discount'	: d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
	},

	discount: function(frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if(d.qty) {
			args = {
				'item_code'			: d.item_code,
				'qty'				: d.qty,
				'unit_price': d.unit_price,
				'discount'	: d.discount,
				'unit_price_out_dcto' : d.unit_price_out_dcto,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_discount",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}
	},
	unit_price_out_dcto: function(frm, cdt, cdn) {
		var item = frappe.get_doc(cdt, cdn);
		item.unit_price = item.unit_price_out_dcto;
		refresh_field("items");
		cur_frm.refresh_fields();

		var d = locals[cdt][cdn];
		if(d.item_code){
			args = {
				'item_code'			: d.item_code,
				'qty'				: d.qty,
				'unit_price': d.unit_price,
				'discount'	: d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function(r) {
					if(r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function(k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}
	}
})

var calculate_base_imponible = function(frm) {
	var doc = frm.doc;
	doc.base_imponible = 0;
	doc.total_taxes = 0;
	doc.total = 0;

	if(doc.items) {
		$.each(doc.items, function(index, data){
			doc.base_imponible += (data.unit_price * data.qty);
			doc.total_taxes += (data.unit_price_with_tax - data.unit_price) * data.qty;
		})
        doc.base_imponible = Math.round(doc.base_imponible * 100) / 100
		doc.total_taxes = Math.round(doc.total_taxes * 100) / 100
		doc.total += doc.base_imponible + doc.total_taxes;
		doc.total = Math.round(doc.total * 100) / 100
	}


	doc.residual_amount = doc.total;
	refresh_field('residual_amount')
	refresh_field('base_imponible')
	refresh_field('total_taxes')
	refresh_field('total')
}
